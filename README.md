# 3D_GameDev

3D játékfejlesztés

Készítő: Iványi Tamás Sándor (Egyedül)
Neptun-kód:GR9AWC

Irányítás:
Játék elindításakor a -Play gombra kattintva elindul az intró. Miután a játék irányíthatóvá vált,
W, A, S, D billentyűk lenyomásával lehet mozgatni a karaktert. A cél, hogy a zuhanás közben felbukkanó akadályokat
kikerülve, minél több pontot érjen el a játékos. A kis karikákon áthaladva dupla pont szerezhető.

Megjegyzés:
A játékon belüli összes modell, és animáció saját készítésű :)
