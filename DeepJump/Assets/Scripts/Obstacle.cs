using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : Initializable
{
    protected GameObject _player;
    protected override bool OnInit()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        if(_player == null)
        {
            return false;
        }
        return true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.Instance.GameActive)
        {
            if (other.gameObject == _player)
            {
                _player.GetComponent<Controller>().Clash();
            }
        }
    }
        
}
