using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Spawner : Initializable
{
    private List<GameObject> _spawnPool;
    [SerializeField]
    private GameObject _pool;
    protected GameManager _manager;

    private IEnumerator _spawnCycle;
    #region Properties

    #endregion

    //temp
    [SerializeField]
    private float _spawTime;
    [SerializeField]
    private float _spawnSpeed;
    protected override bool OnInit()
    {
        _spawnPool = new List<GameObject>();
        if(_pool == null)
        {
            return false;
        }
        
        if(_spawnPool == null)
        {
            return false;
        }

        _spawnCycle = SpawnCycle();

        InitSpawn();
        _manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        if (_manager = null)
        {
            return false;
        }
        GameManager.Instance.EndEvent.AddListener(this.GameEnded);
        GameManager.Instance.StartEvent.AddListener(this.GameStarted);
        GameManager.Instance.ResetEvent.AddListener(this.ResetAll);

        return true;
    }

    private void GameEnded()
    {
        StopCoroutine(_spawnCycle);
    }

    private void ResetAll()
    {
        InitSpawn();
    }

    private void InitSpawn()
    {
        foreach (Transform child in _pool.transform)
        {
            Spawnable x;
            if (child.gameObject.TryGetComponent(out x))
            {
                Debug.Log(child);
                _spawnPool.Add(child.gameObject);
                child.gameObject.SetActive(false);
            }
        }
    }

    private void Spawn()
    {
        var inactives = _spawnPool.Where(x => !x.activeSelf).ToList();
        var spawnId = Random.Range(0, inactives.Count - 1);
        inactives[spawnId].SetActive(true);
        inactives[spawnId].GetComponent<Spawnable>().Launch(_spawnSpeed,transform.position);
    }

    private void GameStarted()
    {
        _spawnCycle = SpawnCycle();
        StartCoroutine(_spawnCycle);
    }

    IEnumerator SpawnCycle()
    {
        yield return new WaitForSeconds(_spawTime);
        Spawn();
        if (GameManager.Instance.GameActive)
        {
            _spawnCycle = SpawnCycle();
            StartCoroutine(_spawnCycle);
        }
        
    }

}
