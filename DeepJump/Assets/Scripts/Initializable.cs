using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Initializable : MonoBehaviour
{
    protected abstract bool OnInit();

    protected void Start()
    {
        if (!OnInit())
        {
            Debug.LogError($"Failed to initialize component {this} on {this.gameObject}");
        }
    }
}
