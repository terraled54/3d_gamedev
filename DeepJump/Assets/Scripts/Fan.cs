using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan : DefaultSpawnable
{
    [SerializeField]
    private GameObject _fan;

    //temp
    [SerializeField]
    private float _rotateSpeed;
    protected override bool OnInit()
    {
        if(_fan == null)
        {
            return false;
        }
        return base.OnInit();
    }

    protected override void FixedUpdate()
    {
        RotateFan();
        base.FixedUpdate();
    }

    private void RotateFan()
    {
        _fan.transform.RotateAround(_fan.transform.position, Vector3.up, _rotateSpeed * Time.fixedDeltaTime);
    }

}
