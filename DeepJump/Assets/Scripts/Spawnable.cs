using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawnable : Initializable
{
    protected float _speed;
    public abstract void Launch(float speed, Vector3 pos);
}
