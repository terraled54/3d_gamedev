using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartButtonController : Initializable
{
    private Button _button;
    private Image _image;
    private Text _text;
    private GameManager _manager;
    protected override bool OnInit()
    {
        if(!TryGetComponent(out _button)){
            return false;
        }
        if (!TryGetComponent(out _image))
        {
            return false;
        }
        _text = GetComponentInChildren<Text>();
        if(_text == null)
        {
            return false;
        }
        _button = GetComponent<Button>();
        _manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        if (_manager = null)
        {
            return false;
        }
        GameManager.Instance.EndEvent.AddListener(this.GameEnded);
        GameManager.Instance.IntroEvent.AddListener(this.GameStarted);
        _text.text = "Play";
        return true;
    }

    public void Click()
    {
        if (GameManager.Instance.GameEnden)
        {
            GameManager.Instance.ResetGame();
            _text.text = "Play";
        }
        else
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<IntroMover>().StartIntro();
        }
    }

    private void GameStarted()
    {
        _button.enabled = false;
        _image.enabled = false;
        _text.enabled = false;
    }

    private void GameEnded()
    {
        _button.enabled = true;
        _image.enabled = true;
        _text.enabled = true;
        _text.text = "Retry";
    }
}
