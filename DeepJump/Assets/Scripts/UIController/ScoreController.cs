using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : Initializable
{
    [SerializeField]
    private Text _score;
    [SerializeField]
    private Text _latestScore;
    [SerializeField]
    private Text _latestScoreText;
    private GameManager _manager;
    protected override bool OnInit()
    {
        if(_score == null || _latestScoreText == null || _latestScore == null)
        {
            return false;
        }
        GameEnded();

        _manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        if (_manager = null)
        {
            return false;
        }
        GameManager.Instance.EndEvent.AddListener(this.GameEnded);
        GameManager.Instance.StartEvent.AddListener(this.GameStarted);
        GameManager.Instance.ScoreChangedEvent.AddListener(this.ScoreChanged);

        return true;
    }

    private void GameStarted()
    {
        _score.enabled = true;
        _latestScore.enabled = false;
        _latestScoreText.enabled = false;
    }

    private void GameEnded()
    {
        _score.enabled = false;
        _latestScore.enabled = true;
        _latestScoreText.enabled = true;
    }

    private void ScoreChanged()
    {
        _latestScore.text = GameManager.Instance.LastScore.ToString();
        _score.text = GameManager.Instance.Score.ToString();
    }
}
