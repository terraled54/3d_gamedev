using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slicer : DefaultSpawnable
{
    [SerializeField]
    private GameObject _blade;
    [SerializeField]
    private Transform _stop1;
    [SerializeField]
    private Transform _stop2;

    private Transform _targetStop;

    //temp
    [SerializeField]
    private float _moveSpeed;
    protected override bool OnInit()
    {
        if (_blade == null)
        {
            return false;
        }
        if (_stop1 == null)
        {
            return false;
        }
        if (_stop2 == null)
        {
            return false;
        }

        _targetStop = _stop1;
        return base.OnInit();
    }

    protected override void FixedUpdate()
    {
        MoveBlade();
        base.FixedUpdate();
    }

    private void MoveBlade()
    {
        var dir = _targetStop.position - _blade.transform.position;

        if(dir.magnitude < .1f)
        {
            if(_targetStop == _stop1)
            {
                _targetStop = _stop2;
            }
            else
            {
                _targetStop = _stop1;
            }
        }

        _blade.transform.position = _blade.transform.position + dir.normalized * _moveSpeed * Time.fixedDeltaTime;
    }
}
