using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLoop : Spawnable
{
    protected GameManager _manager;
    public override void Launch(float speed, Vector3 pos)
    {
        if (GameManager.Instance.GameActive)
        {
            var scale = Random.Range(.3f, 1f);
            transform.localScale = new Vector3(scale, scale, scale);
            transform.rotation = Quaternion.Euler(new Vector3(0, Random.Range(0f, 360f), 0f));
            transform.position = pos + new Vector3(Random.Range(-1f, .1f), 0, Random.Range(-1f, 1f));
            _speed = speed;
        }
       
    }

    protected override bool OnInit()
    {
        _manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        if (_manager = null)
        {
            return false;
        }
        GameManager.Instance.EndEvent.AddListener(this.GameEnded);
        return true;    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        transform.position = transform.position + new Vector3(0f, _speed, 0f) * _speed * Time.fixedDeltaTime;

        if (transform.position.y > 20)
        {
            this.gameObject.SetActive(false);
        }
    }

    protected void GameEnded()
    {
        _speed = 0;
    }
}
