using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerAnimationEventHandler : MonoBehaviour
{
    public UnityEvent JumpUnityEvent;
    public UnityEvent IntroEndedUnityEvent;
    public UnityEvent StepUnityEvent;

    private void Awake()
    {
        JumpUnityEvent = new UnityEvent();
        IntroEndedUnityEvent = new UnityEvent();
    }

    public void JumpEvent()
    {
        JumpUnityEvent.Invoke();
    }

    public void IntroEndedEvent()
    {
        IntroEndedUnityEvent.Invoke();
    }

    public void Step()
    {
        StepUnityEvent?.Invoke();
    }
}
