using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : Initializable
{
    [SerializeField]
    protected ParticleSystem _splash;
    [SerializeField]
    protected SkinnedMeshRenderer _mesh;
    [SerializeField]
    protected AudioSource _splashSound;
    [SerializeField]
    protected AudioSource _stepSound;
    #region Dependencies
    protected CharacterController _controller;
    protected Animator _animator;
    protected GameManager _manager;
    #endregion

    #region Properties
    public bool _inputBlocked = true;

    private float _rawInputX;
    private float _rawInputY;

    private float _inputX;
    private float _inputY;

    private float _speed;
    #endregion


    private void Update()
    {
        GetInputs();
    }
    protected virtual void FixedUpdate()
    {
        Move();
        ClampPosition();
    }

    private void Move()
    {
        var dir = new Vector3(_rawInputX, 0, _rawInputY);       
        if (dir.magnitude > 0)
        {
            if(_speed < 2.5f)
            {
                _speed += .1f;
            }
            
        }
        else
        {
            if(_speed > 0)
            {
                _speed -= .1f;
            }if(_speed < 0)
            {
                _speed = 0;
            }
            
        }
        _controller.Move(dir.normalized * _speed * Time.fixedDeltaTime);
        _animator.SetFloat("FallX", _inputX);
        _animator.SetFloat("FallY", _inputY);
    }

    private void ClampPosition()
    {
        if (GameManager.Instance.GameActive)
        {
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, -1.4f, 1.4f), transform.position.y, Mathf.Clamp(transform.position.z, -1.4f, 1.4f));
        }
      
    }
    private void GetInputs()
    {
        if (!_inputBlocked)
        {
            _rawInputX = Input.GetAxisRaw("Horizontal");
            _rawInputY = Input.GetAxisRaw("Vertical");
            _inputX = Input.GetAxis("Horizontal");
            _inputY = Input.GetAxis("Vertical");
        }
        else
        {
            _rawInputX = 0f;
            _rawInputY = 0f;
        }
    }

    protected override bool OnInit()
    {
        if(!TryGetComponent(out _controller))
        {
            return false;
        }
        _animator = GetComponentInChildren<Animator>();
        if (_animator == null)
        {
            return false;
        }
        if(_splash == null)
        {
            return false;
        }
        if(_mesh == null)
        {
            return false;
        }
        if (_splashSound == null)
        {
            return false;
        }
        if (_stepSound == null)
        {
            return false;
        }

        _manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        if (_manager = null)
        {
            return false;
        }
        _inputBlocked = true;
        GameManager.Instance.EndEvent.AddListener(this.GameEnded);
        GameManager.Instance.StartEvent.AddListener(this.GameStarted);
        return true;
    }

    protected virtual void GameEnded()
    {
        _inputBlocked = true;
    }

    protected virtual void GameStarted()
    {
        _inputBlocked = false;
    }

    public void Clash()
    {
        _mesh.enabled = false;
        _splash.Play();
        _splashSound.Play();
        GameManager.Instance.EndGame();
    }

    public void Score(int score)
    {
        GameManager.Instance.AddScore(score);
    }
}
