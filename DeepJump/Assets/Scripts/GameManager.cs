using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public int LastScore { get; private set; }
    public int Score { get; private set; }
    public bool GameActive { get; private set; }
    public bool GameEnden { get; private set; }

    public UnityEvent IntroEvent;
    public UnityEvent StartEvent;
    public UnityEvent EndEvent;
    public UnityEvent ScoreChangedEvent;
    public UnityEvent ResetEvent;

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
                _instance.IntroEvent = new UnityEvent();
                _instance.StartEvent = new UnityEvent();
                _instance.EndEvent = new UnityEvent();
                _instance.ScoreChangedEvent = new UnityEvent();
                _instance.ResetEvent = new UnityEvent();
            }
            return _instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
        if(_instance != null)
        {
            Destroy(this);
        }
    }

    public void StartIntro()
    {
        _instance.IntroEvent?.Invoke();
    }

    public void StartGame()
    { 
        _instance.StartEvent?.Invoke();
        _instance.GameActive = true;
    }

    public void EndGame()
    {
        _instance.GameActive = false;
        _instance.GameEnden = true;
        _instance.LastScore = Score;
        _instance.Score = 0;
        _instance.EndEvent?.Invoke();
        _instance.ScoreChangedEvent.Invoke();

    }

    public void ResetGame()
    {
        GameEnden = false;
        ResetEvent?.Invoke();
    }

    public void AddScore(int points)
    {
        if (GameActive)
        {
            _instance.Score += points;
            _instance.ScoreChangedEvent.Invoke();
        }
    }
}
