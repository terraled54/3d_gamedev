using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWithIntroController : Initializable
{
    [SerializeField]
    private Vector3 _gameplayPosition;
    [SerializeField]
    private Transform _playerSpine;
    
    private GameObject _player;
    private Vector3 _offset;

    private Vector3 _premiereGoal;

    private bool _gameActive;
    private bool _move;
    private bool _eventsBound;

    //temp
    [SerializeField]
    private float _premiereSpeed;
    protected override bool OnInit()
    {
        if(_playerSpine == null)
        {
            return false;
        }
        
        var manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        if(manager == null)
        {
            return false;
        }

        GameManager.Instance.StartEvent.AddListener(OnGameStarted);
        GameManager.Instance.EndEvent.AddListener(OnGameEnded);
        GameManager.Instance.ResetEvent.AddListener(SetPremiereOffset);

        _player = GameObject.FindGameObjectWithTag("Player");
        if(_player == null)
        {
            return false;
        }

        var playerController = _player.GetComponent<IntroMover>();
        if (playerController == null)
        {
            return false;
        }

        SetPremiereOffset();  

        return true;
    }

    private void InitOffset(Vector3 position, Vector3 rotation)
    {
        transform.position = position;
        //transform.rotation = Quaternion.LookRotation(rotation);
        transform.LookAt(rotation);
        _offset = _player.transform.position - transform.position;
    }

    private void LateUpdate()
    {
        if (!_eventsBound)
        {
            BindeEvents();
        }
        AdjustDistance(_gameActive);
    }

    private void BindeEvents()
    {
        var playerController = _player.GetComponent<IntroMover>();
        _eventsBound = true;
    }

    private void AdjustDistance(bool _activeGame)
    {
        if (_activeGame)
        {
            GameplayAdjust();
        }
        else
        {
            if (_player.GetComponent<IntroMover>().IntroActive)
            {
                IntroAdjust();
            }
            else if (_player.GetComponent<IntroMover>().IntroJumpActive)
            {
                IntroJumpAdjust();
            }
            else
            {
                PremiereAdjust();
            }
        }

       
    }

    private void GameplayAdjust()
    {
        var currentOffsetX = _player.transform.position.x - transform.position.x;
        var currentOffsetZ = _player.transform.position.z - transform.position.z;

        if (Mathf.Abs(currentOffsetX - _offset.x) > .2f || Mathf.Abs(currentOffsetZ - _offset.z) > .2f)
        {
            _move = true;

        }

        if (Mathf.Abs(currentOffsetX - _offset.x) > .05f && Mathf.Abs(currentOffsetZ - _offset.z) < .05f)
        {
            _move = false;

        }

        if (_move)
        {
            transform.position = Vector3.Lerp(transform.position, _player.transform.position - _offset, .005f);
        }
    }

    private void PremiereAdjust()
    {
        if(Mathf.Abs((transform.position - _premiereGoal).magnitude) > .1f)
        {
            transform.position = Vector3.Lerp(transform.position, _premiereGoal, _premiereSpeed * Time.deltaTime);
            //Debug.Log(Mathf.Abs((transform.position - _premiereGoal).magnitude));
        }
        else
        {
            SetPremiereOffset();
        }
        transform.LookAt(_player.transform);
    }

    private void SetPremiereOffset()
    {
        float x;
        x = _player.transform.position.x + Random.Range(-1f, 1f);
        while (Mathf.Abs(_player.transform.position.x - x) < .5f)
        {
            x = _player.transform.position.x + Random.Range(-1f, 1f);
        }

        float y;
        y = _player.transform.position.y + Random.Range(0.5f, 1f);
        while (Mathf.Abs(_player.transform.position.y - y) < .5f)
        {
            y = _player.transform.position.y + Random.Range(0.5f, 1f);
        }

        float z;
        z = _player.transform.position.z + Random.Range(-1f, 1f);
        while (Mathf.Abs(_player.transform.position.z - z) < .5f)
        {
            z = _player.transform.position.z + Random.Range(-1f, 1f);
        }

        var pos = new Vector3(x, y, z);

        float goalX;
        goalX = Random.Range(pos.x - 1f, pos.x + 1f);
        while (Mathf.Abs(pos.x - goalX) < .5f)
        {
            goalX = Random.Range(pos.x - 1f, pos.x + 1f);
        }

        float goalY;
        goalY = Random.Range(pos.y + 0.5f, pos.y + 1f);
        while (Mathf.Abs(pos.y - goalY) < .5f)
        {
            goalY = Random.Range(pos.y + 0.5f, pos.y + 1f);
        }

        float goalZ;
        goalZ = Random.Range(pos.z - 1f, pos.z + 1f);
        while (Mathf.Abs(pos.z - goalZ) < .5f)
        {
            goalZ = Random.Range(pos.z - 1f, pos.z + 1f);
        }

        _premiereGoal = new Vector3(goalX,goalY,goalZ);
        InitOffset(pos, Vector3.zero);
    }

    private void IntroAdjust()
    {
        InitOffset(_playerSpine.position - new Vector3(0, 0, +1),Vector3.zero);
        transform.LookAt(_player.transform);
    }

    private void IntroJumpAdjust()
    {
        transform.position = Vector3.Lerp(transform.position, _gameplayPosition, _premiereSpeed * 10 * Time.deltaTime);
        transform.LookAt(_player.transform);
    }


    private void OnGameStarted() {


        InitOffset(_gameplayPosition, Vector3.down);
        _gameActive = true;

    }

    private void OnGameEnded()
    {

        _gameActive = false;
        SetPremiereOffset();

    }

}
