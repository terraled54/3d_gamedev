using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingDeskController : Initializable
{
    [SerializeField]
    private Vector3 _startingPosition;
    [SerializeField]
    private Vector3 _destination;
    [SerializeField]
    private MeshRenderer _renderer;

    private bool _move;
    //temp
    [SerializeField]
    private float _moveSpeed;
    protected override bool OnInit()
    {
        var intro = GameObject.FindGameObjectWithTag("Player").GetComponent<IntroMover>();
        if(intro == null)
        {
            return false;
        }
        if(_renderer == null)
        {
            return false;
        }

        intro.JumpEvent.AddListener(OnJumpEvent);
        GameManager.Instance.ResetEvent.AddListener(OnIntroStarted);

        ResetPosition();
        return true;
    }


    private void ResetPosition()
    {
        _renderer.enabled = true;
        transform.position = _startingPosition;
    }
    private void FixedUpdate()
    {
        if (_move)
        {
            MoveBackPlatform();
        }
    }

    private void MoveBackPlatform()
    {
        
        transform.position = Vector3.Lerp(transform.position, _destination, _moveSpeed * Time.fixedDeltaTime);
        if(Mathf.Abs((transform.position - _destination).magnitude) < .1f)
        {
            _renderer.enabled = false;
            _move = false;
        }
    }

    private void OnIntroStarted()
    {
        _move = false;
        ResetPosition();
    }

    private void OnJumpEvent()
    {
        _move = true;
    }
}
