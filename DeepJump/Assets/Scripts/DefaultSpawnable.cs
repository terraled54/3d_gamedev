using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultSpawnable : Spawnable
{
    protected GameManager _manager;
    public override void Launch(float speed,Vector3 pos)
    {
        if (GameManager.Instance.GameActive)
        {
            transform.rotation = Quaternion.Euler(new Vector3(-90f, Random.Range(0f, 360f), 0f));
            transform.position = pos;
            _speed = speed;
        }
        
    }

    protected override bool OnInit()
    {
        _manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        if(_manager = null)
        {
            return false;
        }
        GameManager.Instance.EndEvent.AddListener(this.GameEnded);
        return true;   
    }

    protected virtual void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        transform.position = transform.position + new Vector3(0f, _speed, 0f) * _speed * Time.fixedDeltaTime;

        if(transform.position.y > 20)
        {
            this.gameObject.SetActive(false);
        }
    }

    protected void GameEnded()
    {
        _speed = 0;
    }
}
