using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IntroMover : Controller
{
    [SerializeField]
    private Vector3 _startingPosition;
    [SerializeField]
    private AudioSource _wooshSound;
    public bool IntroActive { get; private set; }
    public bool IntroJumpActive { get; private set; }
    public bool IntroEnabled { get; private set; }


    private UnityEvent _jumpEvent;
    public UnityEvent JumpEvent
    {
        get
        {
            if (_jumpEvent == null)
            {
                _jumpEvent = new UnityEvent();
            }
            return _jumpEvent;
        }
    }

    public PlayerAnimationEventHandler AnimationEventHandler { get; private set; }


    //temp
    [SerializeField]
    private float introRunSpeed;
    [SerializeField]
    private float introJumpSpeed;

    protected override bool OnInit()
    {
      
        if(base.OnInit())
        {
            AnimationEventHandler = GetComponentInChildren<PlayerAnimationEventHandler>();
            if(AnimationEventHandler == null)
            {
                return false;
            }
            SetupEvent();
            GameManager.Instance.ResetEvent.AddListener(OnReset);
            AnimationEventHandler.JumpUnityEvent.AddListener(OnIntroJump);
            AnimationEventHandler.IntroEndedUnityEvent.AddListener(EndIntro);
            AnimationEventHandler.StepUnityEvent.AddListener(PlayStep);
            if(_wooshSound == null)
            {
                return false;
            }
            IntroEnabled = true;
            return true;
        }
        else
        {
            return false;
        }    
        
    }

    private void PlayStep()
    {
        _stepSound.Play();
    }

    private void OnReset()
    {
        ResetPosition();
        _splash.gameObject.SetActive(false);
    }

    private void ResetPosition()
    {

        _mesh.enabled = true;
        _animator.SetTrigger("IntroTrigger");
        transform.position = _startingPosition;

    }

    private void SetupEvent()
    {
        var clip = _animator.runtimeAnimatorController.animationClips[7];
        var evt = new AnimationEvent { time = 0.93f };
        evt.functionName = "JumpEvent";

        var evt2 = new AnimationEvent { time = 2.39f };
        evt2.functionName = "IntroEndedEvent";
        clip.AddEvent(evt);
        clip.AddEvent(evt2);
    }
    public void StartIntro()
    {
        if (IntroEnabled)
        {
            _inputBlocked = true;
            IntroActive = true;
            _splash.gameObject.SetActive(true);
            _animator.SetTrigger("IntroTrigger");
            ResetPosition();
            GameManager.Instance.StartIntro();
        }
               
    }

    public void TriggerJump()
    {
        IntroJumpActive = true;
    }

    private void EndIntro()
    {
        IntroEnabled = false;
        IntroActive = false;
        IntroJumpActive = false;
        GameManager.Instance.StartGame();
    }

    protected override void GameEnded()
    {
        IntroEnabled = true;
    }

    protected override void FixedUpdate()
    {
        
        if (IntroActive)
        {
            IntroMove();
        }
        else if (IntroJumpActive)
        {
            IntroJump();
        }
        else if(!_inputBlocked && GameManager.Instance.GameActive)
        {
            base.FixedUpdate();
        }

    }

    private void IntroMove()
    {
        if(transform.position.z < -.9f)
        {
            _controller.Move(Vector3.forward * Time.fixedDeltaTime * introRunSpeed);
        }
        else
        {
            IntroActive = false;
        }
        
    }

    private void IntroJump()
    {
        if (transform.position.z < 0f)
        {
            _controller.Move(Vector3.forward * Time.fixedDeltaTime * introJumpSpeed);
        }
        else
        {
            transform.position = new Vector3(0, transform.position.y, 0);      
        }
    }

    private void OnIntroJump()
    {
        IntroActive = false;
        IntroJumpActive = true;
        _wooshSound.Play();
        JumpEvent?.Invoke();
    }
  
}
