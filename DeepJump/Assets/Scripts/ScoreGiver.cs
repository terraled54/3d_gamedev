using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreGiver : Initializable
{
    protected GameObject _player;

    [SerializeField]
    AudioSource _ding;

    //temp
    [SerializeField]
    int _score;
    protected override bool OnInit()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        if (_player == null)
        {
            return false;
        }
        if(_ding == null)
        {
            return false;
        }
        return true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.Instance.GameActive)
        {
            if (other.gameObject == _player)
            {
                _player.GetComponent<Controller>().Score(_score);
                _ding.Play();
            }
        }
       
    }
}
